Crypty
======

Just a proof of concept for encrypting some files.Easy to use interface.

Easy to use and pretty safe way to encrypt and decrypt sensitive stuff, without undergoing much of trouble.

To use
------

run  `./crypty.sh` then follow the instructions.

For any queries you can reach me here [vinit](http://twitter.com/vinitcool76)
